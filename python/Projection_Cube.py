import numpy as np
import matplotlib.pyplot as plt
import random
import math

# Génération des points
def points(Dim, Nb):
   L = [[random.uniform(-1, 1) for _ in range(Dim)] for _ in range(Nb)]
   return L

# Algorithme de Gram-Schmidt pour créer le plan
def gram_schmidt(A):
   W  = A[0] / np.linalg.norm(A[0])
   e1 = A[1] - (np.dot(A[1],W))*W
   e1 = e1 / np.linalg.norm(e1)
   e2 = A[2] - (np.dot(A[2],W))*W
   e2 = e2 - (np.dot(e2,e1))*e1
   e2 = e2 / np.linalg.norm(e2)
   return [W,e1,e2]

# Fonction de projection sur le plan
def project_on_plane(point, plan):
   y1 = np.dot(point, plan[1])
   y2 = np.dot(point, plan[2])
   return [y1, y2]

# Fonction coordonée du cube de dim N
def generate_cube_coordinates(dimension): # (Non utilisé)
    if dimension < 2:
        return "La dimension doit être d'au moins 2 pour former un cube."

    coordinates = []

    # Initialiser les coordonnées avec les coins opposés du cube
    min_coord = -1
    max_coord = 1


    # Générer les coordonnées du cube
    for i in range(2 ** dimension):
        coord = []
        for j in range(dimension):
            if (i >> j) & 1:
                coord.append(max_coord)
            else:
                coord.append(min_coord)
        coordinates.append(coord)

    return coordinates


def outside_samples(Dim, num_samples, e1, e2):    

    if num_samples >= 2**Dim:
        print("Erreur: Le nombre d'échatillon doit être << 2^Dim")
        return -1

    samples_corner = []
    # Pour un certain nombre d'échantillons << 2^D
    for _ in range(num_samples):
        # 1. choisir 2 valeurs alpha et beta au hasard
        alpha = np.random.uniform(-1, 1)
        beta = np.random.uniform(-1, 1)

        # 2. v = alpha * e1 + beta * e2  # v est dans le plan de projection
        v = alpha * e1 + beta * e2

        # 3. v = v/norm(v)   # v est de norme 1
        v /= np.linalg.norm(v)

        # 4. v[i] = sign(v[i]) pour toutes les coordonnees de v # on transforme v en vecteur de +1 et -1
        v = np.sign(v)

        samples_corner.append(v)
    return np.array(samples_corner)

def Generation(Dim,N,num_samples): # On entre en paramètre la dimension de l'hypercube, le nombre de points, et le nombre d'échantillons des coins à afficher

    # Cube et points
    Vect = gram_schmidt(points(Dim, Dim))

    points_cube = np.array(generate_cube_coordinates(Dim))
    points_cube_project = np.array([project_on_plane(i, Vect) for i in points_cube])
    


    # En ne prenant cette fois ci que un certain nombre d'échantillons de coins 

    samples_points_cube = np.array(outside_samples(Dim, num_samples, Vect[1], Vect[2])) # On utilise 
    samples_points_cube_project = np.array([project_on_plane(i, Vect) for i in samples_points_cube])

    
    points_cube_project=samples_points_cube_project


    Liste_points = np.array(points(Dim, N))
    points_project = np.array([project_on_plane(j, Vect) for j in Liste_points])


    # Cercle inscrit et circonscrit
    a = 1.0
    b = math.sqrt(Dim)

    # Angle theta allant de 0 à 2*pi
    theta_a = np.linspace(0, 2*np.pi, 100)
    theta_b = np.linspace(0, 2*np.pi, 100)

    # Calcul des coordonnées x et y du cercle
    x_inscrit = a * np.cos(theta_a)
    y_inscrit = a * np.sin(theta_a)
    x_circonsrcit = b * np.cos(theta_b)
    y_circonsrcit = b * np.sin(theta_b)

    plt.figure()
    plt.scatter(points_project[:, 0], points_project[:, 1], color='blue', marker='o', s=1)
    plt.scatter(points_cube_project[:, 0], points_cube_project[:, 1], color='red', marker='o', s=1)
    plt.plot(x_inscrit, y_inscrit, color='red')
    plt.plot(x_circonsrcit, y_circonsrcit, color='red')
    plt.title('Dimension = ' + str(Dim) + '   Nombre de points = ' + str(N))
    plt.xlabel('Axe des x')
    plt.ylabel('Axe des y')
    plt.axis('equal')
    plt.show()
    return 0

Generation(3,1000,2**3 -1)
Generation(5,1000,2**5 -1)
Generation(8,1000,2**8 -1)
Generation(10,10000,2**10 -1)
Generation(20,10000,2**20 -1) # ça ne ressemble plus à grand chose